package com.example.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.bean.Contacto;

@Service
public class ContactoServiceImpl implements ContactoService{
	private static final String REST_CONTACTO_URL = "http://localhost:8081/rest/";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public List<Contacto> obtenerContactos() {
		UriComponentsBuilder urlBuilder =  UriComponentsBuilder.fromHttpUrl(REST_CONTACTO_URL);
		urlBuilder.path("contactos");
		Contacto[] contactosVector = restTemplate.getForObject(urlBuilder.toUriString(), Contacto[].class );
		List<Contacto> contactosArrayList = Arrays.asList(contactosVector);
		return contactosArrayList;
	}

	@Override
	public Contacto agregarContacto(Contacto contacto) {
		UriComponentsBuilder urlBuilder =  UriComponentsBuilder.fromHttpUrl(REST_CONTACTO_URL);
		Contacto contactoRespuesta = restTemplate.postForObject(urlBuilder.path("contactos").toUriString(), 
				contacto, Contacto.class);
		return contactoRespuesta;
	}

	@Override
	public Contacto obtenerContacto(Integer id) {
		UriComponentsBuilder urlBuilder =  UriComponentsBuilder.fromHttpUrl(REST_CONTACTO_URL);
		Contacto contacto = restTemplate.getForObject(
				urlBuilder.path("/contactos").path(String.valueOf(id)).toUriString(), Contacto.class);
		return contacto;
	}

	
	
}
